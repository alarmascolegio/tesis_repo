﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TesisAlarmas.Models
{
    public class Nivel
    {
        public string EducacionCodigo { get; set; }
        public int NivelID { get; set; }
        public string NivelNombre { get; set; }

        public static IQueryable<Nivel> GetNiveles()
        {
            return new List<Nivel>
            {
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Básica",
                    NivelID = 1,
                    NivelNombre = "Primero Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Básica",
                    NivelID = 2,
                    NivelNombre = "Segundo Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Básica",
                    NivelID = 3,
                    NivelNombre = "Tercero Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Básica",
                    NivelID = 4,
                    NivelNombre = "Cuarto Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Básica",
                    NivelID = 5,
                    NivelNombre = "Quinto Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Básica",
                    NivelID = 6,
                    NivelNombre = "Sexto Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Básica",
                    NivelID = 7,
                    NivelNombre = "Septimo Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Básica",
                    NivelID = 8,
                    NivelNombre = "Octavo Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Media",
                    NivelID = 9,
                    NivelNombre = "Primero Medio"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Media",
                    NivelID = 10,
                    NivelNombre = "Segundo Medio"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Media",
                    NivelID = 11,
                    NivelNombre = "Tercero Medio"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Media",
                    NivelID = 12,
                    NivelNombre = "Cuarto Medio"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 13,
                    NivelNombre = "Primero Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 14,
                    NivelNombre = "Segundo Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 15,
                    NivelNombre = "Tercero Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 17,
                    NivelNombre = "Cuarto Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 18,
                    NivelNombre = "Quinto Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 19,
                    NivelNombre = "Sexto Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 20,
                    NivelNombre = "Septimo Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 21,
                    NivelNombre = "Octavo Básico"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 22,
                    NivelNombre = "Primero Medio"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 23,
                    NivelNombre = "Segundo Medio"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 24,
                    NivelNombre = "Tercero Medio"
                },
                new Nivel
                {
                    EducacionCodigo = "Enseñanza Mixta",
                    NivelID = 25,
                    NivelNombre = "Cuarto Medio"
                },
            }.AsQueryable();
        }
    }
}