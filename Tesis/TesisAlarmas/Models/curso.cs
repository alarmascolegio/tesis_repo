﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using System.ComponentModel.DataAnnotations;

namespace TesisAlarmas
{
    [MetadataType(typeof(cursoMetadata))]
    public partial class curso
    {
        public bool Validar(string Educacion)
        {
            if (Educacion != null && Educacion != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class cursoMetadata
    {

        //[Required(ErrorMessage = "Por favor seleccione un tipo de Educacion")]
        //public string Educacion { get; set; }

        [Required(ErrorMessage = "Por favor seleccione un Nivel")]
        public string Nivel { get; set; }

        [Required(ErrorMessage = "La división es requerida")]
        [RegularExpression("[A-Z]",ErrorMessage = "La División solo puede ser una letra de A a Z")]
        public string Division { get; set; }
    }
}