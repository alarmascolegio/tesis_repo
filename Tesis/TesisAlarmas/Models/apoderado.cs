﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using TesisAlarmas.Models;

namespace TesisAlarmas
{

    

    [MetadataType(typeof(apoderadoMetadata))]
    public partial class apoderado
    {
        public string GeneratePassword()
        {

            //Since I'm big on security, I wanted to generate passwords that contained numbers, letters and special
            //characters - and not easily hacked.
            //I started with creating three string variables.
            //This one tells you how many characters the string will contain.
            string PasswordLength = "12";
            //This one, is empty for now - but will ultimately hold the finised randomly generated password
            string NewPassword = "";

            //This one tells you which characters are allowed in this new password
            string allowedChars = "";

            allowedChars = "1,2,3,4,5,6,7,8,9,0";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "~,!,@,#,$,%,^,&,*,+,?";

            //Then working with an array...

            char[] sep = { ',' };

            string[] arr = allowedChars.Split(sep);
            string IDString = "";

            string temp = "";

            //utilize the "random" class

            Random rand = new Random();

            //and lastly - loop through the generation process...

            for (int i = 0; i < Convert.ToInt32(PasswordLength); i++)

            {
                temp = arr[rand.Next(0, arr.Length)];
                IDString += temp;

                NewPassword = IDString;
                //For Testing purposes, I used a label on the front end to show me the generated password.
                //lblProduct.Text = IDString;


            }



            return NewPassword;

        }
        public bool Validar(apoderado apoderado)
        {
            string rut = Convert.ToString(apoderado.RUT);


            if (apoderado.RUT != 0 || rut != "")
            {
                if (apoderado.Nombres != "" || apoderado.Nombres != null)
                {
                    if (apoderado.Apellidos != "" || apoderado.Apellidos != null)
                    {

                        if (apoderado.Celular != "" || apoderado.Celular != null)
                        {
                            if (apoderado.Email != "" || apoderado.Email != null)
                            {
                                if (apoderado.Direccion != "" || apoderado.Direccion != null)
                                {
                                    string contra_auto = "";
                                    contra_auto = apoderado.GeneratePassword();
                                    apoderado.Contrasena = contra_auto;
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;

                }
            }
            else
            {
                return false;
            }

            return false;
       }

        public apoderado LoginApoderado(string Usuario, string Contrasena)
        {
            try
            {
                var db = new BD();
                var apoderado = db.apoderado;
                foreach (var usuario in apoderado)
                {
                    if (usuario.RUT == Convert.ToInt32(Usuario) && usuario.Contrasena == Contrasena)
                    {
                        return usuario;
                    }
                    else
                    {
                        return null;
                    }
                }
                
            }
            catch(Exception ex)
            {
                return null;
            }
            return null;
        }

        public apoderado RecuperarContra(string correo)
        {
            var db = new BD();

            apoderado CorreoBusqueda = db.apoderado.Where(model => model.Email == correo).FirstOrDefault();
            
            return CorreoBusqueda;
        }
    }
    public class apoderadoMetadata
    {
        [Required(ErrorMessage = "El RUT es requerido")]  
        public int RUT { get; set; }

        [Required(ErrorMessage = "El nombres es requerido")]
        [StringLength(45, ErrorMessage = "El campo nombre soporta 45 caracteres")]
        public string Nombres { get; set; }

        [Required(ErrorMessage = "Los Apellidos son requeridos")]
        [StringLength(45, ErrorMessage = "El campo apellido soporta 45 caracteres")]
        public string Apellidos { get; set; }

        [Required(ErrorMessage = "El telefono es requerido")]
        [StringLength(45, ErrorMessage = "El campo celular soporta 45 caracteres")]
        public string Celular { get; set; }

        [Required(ErrorMessage = "La dirección es requerido")]
        [StringLength(200, ErrorMessage = "El campo direccion soporta hasta 200 caracteres")]
        public string Direccion { get; set; }

        [Required(ErrorMessage = "El email es requerido")]
        [RegularExpression ("^[a-z0-9_\\+-]+(\\.[a-z0-9\\+-]+)*@[a-z0-9-]+(\\.[a-z0-9]+)*\\.([a-z]{2,4})$", ErrorMessage = "Email no valido")]
        [StringLength(100, ErrorMessage = "El campo email soporta 100 caracteres")]
        public string Email { get; set; }

        
    }
}