﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TesisAlarmas.Models
{
    public class Educacion
    {
        public string EducacionCodigo { get; set; }
        public string EducacionTipo { get; set; }

        public static IQueryable<Educacion> GetEducaciones()
        {
            return new List<Educacion>
            {
                new Educacion{
                    EducacionCodigo = "Enseñanza Básica",
                    EducacionTipo = "Enseñanza Básica"
                },
                new Educacion{
                    EducacionCodigo = "Enseñanza Media",
                    EducacionTipo = "Enseñanza Media"
                },
                new Educacion{
                    EducacionCodigo = "Enseñanza Mixta",
                    EducacionTipo = "Enseñanza Mixta"
                }
            }.AsQueryable();
        }
    }
}