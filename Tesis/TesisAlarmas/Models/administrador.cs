﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TesisAlarmas
{
    public partial class administrador
    {
        public administrador LoginAdministrador(string Usuario, string Contrasena)
        {
            try
            {
                var db = new BD();
                var admin = db.administrador;
                foreach (var usuario in admin)
                {
                    if (usuario.Usuario == Usuario && usuario.Contrasena == Contrasena)
                    {
                        return usuario;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }
    }
}