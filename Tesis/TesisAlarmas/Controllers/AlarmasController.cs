﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TesisAlarmas;

namespace TesisAlarmas.Controllers
{
    public class AlarmasController : Controller
    {
        private BD db = new BD();

        // GET: Alarmas
        public ActionResult Index()
        {
            var alarma = db.alarma.Include(a => a.alumno).Include(a => a.horario);
            return View(alarma.ToList());
        }

        // GET: Alarmas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            alarma alarma = db.alarma.Find(id);
            if (alarma == null)
            {
                return HttpNotFound();
            }
            return View(alarma);
        }

        // GET: Alarmas/Create
        public ActionResult Create()
        {
            ViewBag.Alumno_RUT = new SelectList(db.alumno, "RUT", "Nombres");
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana");
            return View();
        }

        // POST: Alarmas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Horario_ID,Mensaje,Alumno_RUT")] alarma alarma)
        {
            if (ModelState.IsValid)
            {
                db.alarma.Add(alarma);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Alumno_RUT = new SelectList(db.alumno, "RUT", "Nombres", alarma.Alumno_RUT);
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana", alarma.Horario_ID);
            return View(alarma);
        }

        // GET: Alarmas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            alarma alarma = db.alarma.Find(id);
            if (alarma == null)
            {
                return HttpNotFound();
            }
            ViewBag.Alumno_RUT = new SelectList(db.alumno, "RUT", "Nombres", alarma.Alumno_RUT);
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana", alarma.Horario_ID);
            return View(alarma);
        }

        // POST: Alarmas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Horario_ID,Mensaje,Alumno_RUT")] alarma alarma)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alarma).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Alumno_RUT = new SelectList(db.alumno, "RUT", "Nombres", alarma.Alumno_RUT);
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana", alarma.Horario_ID);
            return View(alarma);
        }

        // GET: Alarmas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            alarma alarma = db.alarma.Find(id);
            if (alarma == null)
            {
                return HttpNotFound();
            }
            return View(alarma);
        }

        // POST: Alarmas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            alarma alarma = db.alarma.Find(id);
            db.alarma.Remove(alarma);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
