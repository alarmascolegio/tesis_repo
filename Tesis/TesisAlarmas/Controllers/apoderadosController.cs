﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TesisAlarmas;
using System.Web.Security;
using System.Text.RegularExpressions;

namespace TesisAlarmas.Controllers
{
    public class apoderadosController : Controller
    {
        private BD db = new BD();

        // GET: apoderados
        public ActionResult Index()
        {
            return View(db.apoderado.ToList());
        }

        // GET: apoderados/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            apoderado apoderado = db.apoderado.Find(id);
            if (apoderado == null)
            {
                return HttpNotFound();
            }
            return View(apoderado);
        }

        public ActionResult Menu()
        {
            return View();
        }


        // GET: apoderados/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: apoderados/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.

   

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(apoderado apoderado)
        {
           

            try
            {
                string password = Membership.GeneratePassword(12, 1);
                
                if (ModelState.IsValid)
                {

                    bool a = apoderado.Validar(apoderado);
                    if (a == true)
                    {
                        apoderado.Contrasena = password;
                        db.apoderado.Add(apoderado);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.Message = "no se creó el apoderado, intente nuevamente";
                    }

                }

            }
            catch (Exception Xe)
            {
                ViewBag.Message = "No se creó el apoderado, intente nuevamente "+ Xe;
            }

            return View(apoderado);
        }

        // GET: apoderados/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            apoderado apoderado = db.apoderado.Find(id);
            if (apoderado == null)
            {
                return HttpNotFound();
            }
            return View(apoderado);
        }

        // POST: apoderados/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RUT,Nombres,Apellidos,Celular,Direccion,Email,Contrasena")] apoderado apoderado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(apoderado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(apoderado);
        }

        // GET: apoderados/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            apoderado apoderado = db.apoderado.Find(id);
            if (apoderado == null)
            {
                return HttpNotFound();
            }
            return View(apoderado);
        }

        // POST: apoderados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            apoderado apoderado = db.apoderado.Find(id);
            db.apoderado.Remove(apoderado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
