﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TesisAlarmas.Models;

namespace TesisAlarmas.Controllers
{
    public class CursoController : Controller
    {
        private BD db = new BD();

        // GET: Curso
        public ActionResult Index()
        {
            return View(db.curso.ToList());
        }

        // GET: Curso/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            curso curso = db.curso.Find(id);
            if (curso == null)
            {
                return HttpNotFound();
            }
            return View(curso);
        }

        // GET: Curso/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Curso/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(curso curso, string Educacion)
        {
            bool validacion = curso.Validar(Educacion);

            if (validacion == true)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        db.curso.Add(curso);
                        db.SaveChanges();

                        ViewBag.MessageOK = "El curso " + curso.Nivel + "-" + curso.Division + " se ha agregado correctamente.";
                        return View();
                        //return RedirectToAction("Index");
                    }
                }
                catch(Exception ex)
                {
                    ViewBag.MessageError = "No se ha podido agregar el curso a la Base de Datos. <br/>El error es: "+ex;
                }
            }
            else
            {
                ViewBag.MessageEducacion = "Por favor seleccione un Tipo de Educación";
            }

            return View(curso);
        }

        public ActionResult ListaEducacion()
        {
            IQueryable educaciones = Educacion.GetEducaciones();

            if (HttpContext.Request.IsAjaxRequest())
            {
                return Json(new SelectList(
                            educaciones,
                            "EducacionCodigo",
                            "EducacionTipo"), JsonRequestBehavior.AllowGet
                            );
            }

            return View(educaciones);
        }

        public ActionResult ListaNivel(string EducacionCodigo)
        {
            IQueryable niveles = Nivel.GetNiveles().Where(x => x.EducacionCodigo == EducacionCodigo);

            if (HttpContext.Request.IsAjaxRequest())
                return Json(new SelectList(
                                niveles,
                                "NivelID",
                                "NivelNombre"), JsonRequestBehavior.AllowGet
                            );

            return View(niveles);
        }

        // GET: Curso/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            curso curso = db.curso.Find(id);
            if (curso == null)
            {
                return HttpNotFound();
            }
            return View(curso);
        }

        // POST: Curso/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Nivel,Division")] curso curso)
        {
            if (ModelState.IsValid)
            {
                db.Entry(curso).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(curso);
        }

        // GET: Curso/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            curso curso = db.curso.Find(id);
            if (curso == null)
            {
                return HttpNotFound();
            }
            return View(curso);
        }

        // POST: Curso/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            curso curso = db.curso.Find(id);
            db.curso.Remove(curso);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
