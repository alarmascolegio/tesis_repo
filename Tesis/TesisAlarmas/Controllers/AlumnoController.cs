﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TesisAlarmas;

namespace TesisAlarmas.Controllers
{
    public class AlumnoController : Controller
    {
        private BD db = new BD();

        // GET: Alumno
        public ActionResult Index()
        {
            var alumno = db.alumno.Include(a => a.apoderado).Include(a => a.curso);
            return View(alumno.ToList());
        }
        

        // GET: Alumno/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            alumno alumno = db.alumno.Find(id);
            if (alumno == null)
            {
                return HttpNotFound();
            }
            return View(alumno);
        }

        // GET: Alumno/Create
        public ActionResult Create()
        {
            ViewBag.Apoderado_RUT = new SelectList(db.apoderado, "RUT", "Nombres");
            ViewBag.Curso_ID = new SelectList(db.curso, "ID", "Nivel");
            return View();
        }

        // POST: Alumno/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RUT,Nombres,Apellidos,Celular,Direccion,Email,Apoderado_RUT,Curso_ID,Ubicacion_ID")] alumno alumno)
        {
            if (ModelState.IsValid)
            {
                db.alumno.Add(alumno);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Apoderado_RUT = new SelectList(db.apoderado, "RUT", "Nombres", alumno.Apoderado_RUT);
            ViewBag.Curso_ID = new SelectList(db.curso, "ID", "Nivel", alumno.Curso_ID);
            return View(alumno);
        }

        // GET: Alumno/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            alumno alumno = db.alumno.Find(id);
            if (alumno == null)
            {
                return HttpNotFound();
            }
            ViewBag.Apoderado_RUT = new SelectList(db.apoderado, "RUT", "Nombres", alumno.Apoderado_RUT);
            ViewBag.Curso_ID = new SelectList(db.curso, "ID", "Nivel", alumno.Curso_ID);
            return View(alumno);
        }

        // POST: Alumno/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RUT,Nombres,Apellidos,Celular,Direccion,Email,Apoderado_RUT,Curso_ID,Ubicacion_ID")] alumno alumno)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alumno).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Apoderado_RUT = new SelectList(db.apoderado, "RUT", "Nombres", alumno.Apoderado_RUT);
            ViewBag.Curso_ID = new SelectList(db.curso, "ID", "Nivel", alumno.Curso_ID);
            return View(alumno);
        }

        // GET: Alumno/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            alumno alumno = db.alumno.Find(id);
            if (alumno == null)
            {
                return HttpNotFound();
            }
            return View(alumno);
        }

        // POST: Alumno/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            alumno alumno = db.alumno.Find(id);
            db.alumno.Remove(alumno);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
