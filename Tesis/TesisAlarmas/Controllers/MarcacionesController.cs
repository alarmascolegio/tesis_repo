﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TesisAlarmas;

namespace TesisAlarmas.Controllers
{
    public class MarcacionesController : Controller
    {
        private BD db = new BD();

        // GET: Marcaciones
        public ActionResult Index()
        {
            var marcaciones = db.marcaciones.Include(m => m.alumno);
            return View(marcaciones.ToList());
        }

        // GET: Marcaciones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            marcaciones marcaciones = db.marcaciones.Find(id);
            if (marcaciones == null)
            {
                return HttpNotFound();
            }
            return View(marcaciones);
        }

        // GET: Marcaciones/Create
        public ActionResult Create()
        {
            ViewBag.Alumno_RUT = new SelectList(db.alumno, "RUT", "Nombres");
            return View();
        }

        // POST: Marcaciones/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Fecha,Estado,Tipo_Marcacion,Observaciones,Alumno_RUT")] marcaciones marcaciones)
        {
            if (ModelState.IsValid)
            {
                db.marcaciones.Add(marcaciones);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Alumno_RUT = new SelectList(db.alumno, "RUT", "Nombres", marcaciones.Alumno_RUT);
            return View(marcaciones);
        }

        // GET: Marcaciones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            marcaciones marcaciones = db.marcaciones.Find(id);
            if (marcaciones == null)
            {
                return HttpNotFound();
            }
            ViewBag.Alumno_RUT = new SelectList(db.alumno, "RUT", "Nombres", marcaciones.Alumno_RUT);
            return View(marcaciones);
        }

        // POST: Marcaciones/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Fecha,Estado,Tipo_Marcacion,Observaciones,Alumno_RUT")] marcaciones marcaciones)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marcaciones).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Alumno_RUT = new SelectList(db.alumno, "RUT", "Nombres", marcaciones.Alumno_RUT);
            return View(marcaciones);
        }

        // GET: Marcaciones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            marcaciones marcaciones = db.marcaciones.Find(id);
            if (marcaciones == null)
            {
                return HttpNotFound();
            }
            return View(marcaciones);
        }

        // POST: Marcaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            marcaciones marcaciones = db.marcaciones.Find(id);
            db.marcaciones.Remove(marcaciones);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
