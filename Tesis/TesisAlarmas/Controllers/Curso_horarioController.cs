﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TesisAlarmas;

namespace TesisAlarmas.Controllers
{
    public class Curso_horarioController : Controller
    {
        private BD db = new BD();

        // GET: Curso_horario
        public ActionResult Index()
        {
            var curso_horario = db.curso_horario.Include(c => c.curso).Include(c => c.horario);
            return View(curso_horario.ToList());
        }

        // GET: Curso_horario/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            curso_horario curso_horario = db.curso_horario.Find(id);
            if (curso_horario == null)
            {
                return HttpNotFound();
            }
            return View(curso_horario);
        }

        // GET: Curso_horario/Create
        public ActionResult Create()
        {
            ViewBag.Curso_ID = new SelectList(db.curso, "ID", "Nivel");
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana");
            return View();
        }

        // POST: Curso_horario/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Curso_ID,Horario_ID,ID")] curso_horario curso_horario)
        {
            if (ModelState.IsValid)
            {
                db.curso_horario.Add(curso_horario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Curso_ID = new SelectList(db.curso, "ID", "Nivel", curso_horario.Curso_ID);
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana", curso_horario.Horario_ID);
            return View(curso_horario);
        }

        // GET: Curso_horario/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            curso_horario curso_horario = db.curso_horario.Find(id);
            if (curso_horario == null)
            {
                return HttpNotFound();
            }
            ViewBag.Curso_ID = new SelectList(db.curso, "ID", "Nivel", curso_horario.Curso_ID);
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana", curso_horario.Horario_ID);
            return View(curso_horario);
        }

        // POST: Curso_horario/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Curso_ID,Horario_ID,ID")] curso_horario curso_horario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(curso_horario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Curso_ID = new SelectList(db.curso, "ID", "Nivel", curso_horario.Curso_ID);
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana", curso_horario.Horario_ID);
            return View(curso_horario);
        }

        // GET: Curso_horario/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            curso_horario curso_horario = db.curso_horario.Find(id);
            if (curso_horario == null)
            {
                return HttpNotFound();
            }
            return View(curso_horario);
        }

        // POST: Curso_horario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            curso_horario curso_horario = db.curso_horario.Find(id);
            db.curso_horario.Remove(curso_horario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
