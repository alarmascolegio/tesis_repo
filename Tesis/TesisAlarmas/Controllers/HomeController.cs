﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TesisAlarmas.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //var db = new BD();
            //var usuarios = from u in db.administrador
            //               select u;

            //foreach (var usuario in usuarios)
            //{
            //    var nombre = usuario.Usuario;
            //}

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}