﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TesisAlarmas;

namespace TesisAlarmas.Controllers
{
    public class Horario_personalizadoController : Controller
    {
        private BD db = new BD();

        // GET: Horario_personalizado
        public ActionResult Index()
        {
            var horario_personalizado = db.horario_personalizado.Include(h => h.apoderado).Include(h => h.horario);
            return View(horario_personalizado.ToList());
        }

        // GET: Horario_personalizado/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            horario_personalizado horario_personalizado = db.horario_personalizado.Find(id);
            if (horario_personalizado == null)
            {
                return HttpNotFound();
            }
            return View(horario_personalizado);
        }

        // GET: Horario_personalizado/Create
        public ActionResult Create()
        {
            ViewBag.Apoderado_RUT = new SelectList(db.apoderado, "RUT", "Nombres");
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana");
            return View();
        }

        // POST: Horario_personalizado/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,HoraEntradaManana,HoraSalidaManana,HoraEntradaTarde,HoraSalidaTarde,Apoderado_RUT,Horario_ID")] horario_personalizado horario_personalizado)
        {
            if (ModelState.IsValid)
            {
                db.horario_personalizado.Add(horario_personalizado);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Apoderado_RUT = new SelectList(db.apoderado, "RUT", "Nombres", horario_personalizado.Apoderado_RUT);
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana", horario_personalizado.Horario_ID);
            return View(horario_personalizado);
        }

        // GET: Horario_personalizado/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            horario_personalizado horario_personalizado = db.horario_personalizado.Find(id);
            if (horario_personalizado == null)
            {
                return HttpNotFound();
            }
            ViewBag.Apoderado_RUT = new SelectList(db.apoderado, "RUT", "Nombres", horario_personalizado.Apoderado_RUT);
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana", horario_personalizado.Horario_ID);
            return View(horario_personalizado);
        }

        // POST: Horario_personalizado/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,HoraEntradaManana,HoraSalidaManana,HoraEntradaTarde,HoraSalidaTarde,Apoderado_RUT,Horario_ID")] horario_personalizado horario_personalizado)
        {
            if (ModelState.IsValid)
            {
                db.Entry(horario_personalizado).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Apoderado_RUT = new SelectList(db.apoderado, "RUT", "Nombres", horario_personalizado.Apoderado_RUT);
            ViewBag.Horario_ID = new SelectList(db.horario, "ID", "DiaDeSemana", horario_personalizado.Horario_ID);
            return View(horario_personalizado);
        }

        // GET: Horario_personalizado/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            horario_personalizado horario_personalizado = db.horario_personalizado.Find(id);
            if (horario_personalizado == null)
            {
                return HttpNotFound();
            }
            return View(horario_personalizado);
        }

        // POST: Horario_personalizado/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            horario_personalizado horario_personalizado = db.horario_personalizado.Find(id);
            db.horario_personalizado.Remove(horario_personalizado);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
