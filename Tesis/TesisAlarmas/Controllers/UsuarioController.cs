﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TesisAlarmas;
using System.Net;
using System.Net.Mail;

namespace Tesis.Controllers
{
    public class UsuarioController : Controller
    {
        public ActionResult Menu()
        {
            return View();
        }

        public ActionResult Login()
        {
            if (Session["adminId"] != null)
            {
                return Redirect("/Home/About");
            }
            else
            {
                if(Session["apoderadoRUT"] != null)
                {
                    return Redirect("/Home/Contact");
                }
                return View();
            }
        }

        [HttpPost]
        public ActionResult Login(string Usuario, string Contrasena)
        {
            ViewBag.Usuario = Usuario.Trim();
            ViewBag.Contrasena = Contrasena.Trim();

            if ((Usuario != null && Usuario != "") && (Contrasena != null && Contrasena != ""))
            {
                try
                {
                    var administrador = new administrador();

                    administrador busqueda1 = administrador.LoginAdministrador(Usuario, Contrasena);
                    if (busqueda1 != null)
                    {
                        Session.Add("AdminID", busqueda1.ID);
                        Session.Add("UsuarioNombre", busqueda1.Usuario);
                        Session.Add("Rol", "Administrador");
                        return Redirect("/Usuario/Menu");
                    }
                    else
                    {
                        var apoderado = new apoderado();

                        apoderado busqueda2 = apoderado.LoginApoderado(Usuario, Contrasena);
                        if (busqueda2 != null)
                        {
                            Session.Add("ApoderadoRUT", busqueda2.RUT);
                            Session.Add("UsuarioNombre", busqueda2.Nombres);
                            Session.Add("UsuarioApellido", busqueda2.Apellidos);
                            Session.Add("Rol", "Apoderado");
                            return Redirect("/apoderados/Menu");
                        }
                        else
                        {
                            ViewBag.MessageError = "Error al ingresar datos";
                        }
                    }
                }
                catch (Exception errorlogin)
                {
                    ViewBag.MessageError = "Ha ocurrido un error: " + errorlogin;
                }
            } 
            else
            {
                    ViewBag.MessageError = "Por favor ingrese datos";
            }
            return View();
        }



        public ActionResult Recuperar()
        {
            ViewBag.Message = "Email";

            return View();
        }

        [HttpPost]
        public ActionResult Recuperar(string Correo)
        {
            ViewBag.Correo = Correo;

            var apoderado = new apoderado();
            apoderado contrasena = apoderado.RecuperarContra(Correo);
            var mmsg = new MailMessage();
            mmsg.To.Add(new MailAddress(Correo));
            mmsg.Body = "Señor/a "+contrasena.Nombres+" "+contrasena.Apellidos+" su contraseña es: "+contrasena.Contrasena;
            mmsg.Subject = "Recuperación de contraseña";
            mmsg.SubjectEncoding = System.Text.Encoding.UTF8;
            mmsg.From = new MailAddress("m1m2m3m4m5@outlook.com");
            mmsg.IsBodyHtml = true;

            using (var cliente = new SmtpClient())
            {
                try
                {
                    cliente.Send(mmsg);
                    return Redirect("/Usuario/Login");
                }
                catch (SmtpException ex)
                {
                    ViewBag.Message = "Ha ocurrido un error: " + ex;
                }
            }
            return View();
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return Redirect("/Usuario/Login");
        }
    }
}